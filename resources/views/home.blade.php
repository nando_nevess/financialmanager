@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('css')
  <!-- iCheck for checkboxes and radio inputs -->
  <link rel="stylesheet" href="{!! asset('/plugins/iCheck/all.css') !!}">
@stop

@section('content_header')
    <h1>Dashboard</h1>
@stop

@section('content')

<div class="content">
        <!-- /.row -->
        <div class="row">
    
            <!-- /.Current Balance-box -->
            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-blue"></span>
                    
                    <div class="info-box-content">
                        <span class="info-box-text">Current Balance</span>
                        <span class="info-box-number">BRL 1.130,00</span>
                    </div>
                </div>
            </div>
            <!-- /.Current Balance-box -->
            
            <!-- /.Incomes-box -->
            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-green"></span>
    
                    <div class="info-box-content">
                        <span class="info-box-text">Incomes</span>
                        <span class="info-box-number">BRL 5.000,00</span>
                    </div>
                </div>
            </div>
            <!-- /.Incomes-box -->
    
            <!-- /.Expenses-box -->
            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-red"></span>
    
                    <div class="info-box-content">
                        <span class="info-box-text">Expenses</span>
                        <span class="info-box-number">BRL 3.870,00</span>
                    </div>
                </div>
            </div>
            <!-- /.Expenses-box -->
    
        </div>
        <!-- /.row -->
    
        <!-- /.row -->
        <div class="row">
    
            <!-- /.Actions-btns -->
            <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Block Buttons</h3>
                    </div>
                    
                    <div class="box-body">
                        <button type="button" class="btn btn-default btn-block bg-green">Create income</button>
                        <button type="button" class="btn btn-default btn-block bg-red">Create expense</button>
                    </div>
                </div>
            </div>
            <!-- /.Actions-btns -->
    
            <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="box">
                    <form class="form-horizontal">
                        
                        <!-- /.box-header -->
                        <div class="box-header with-border">
                            <h3 class="box-title">Horizontal Form</h3>
                        </div>
                        <!-- /.box-header -->
                        
                        <!-- /.box-body -->
                        <div class="box-body">
                            <div class="form-group">
                                <label for="" class="col-sm-3 control-label">Value</label>
    
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" id="money">
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label for="" class="col-sm-3 control-label">Date</label>
    
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" id="datemask">
                                </div>
                            </div>
    
                            <div class="form-group">
                                <label for="" class="col-sm-3 control-label">Description</label>
    
                                <div class="col-sm-9">
                                    <input type="" class="form-control" id="" placeholder="">
                                </div>
                            </div>
    
                            <div class="form-group">
                                <label for="" class="col-sm-3 control-label">Category</label>
    
                                <div class="col-sm-9">
                                    <input type="" class="form-control" id="" placeholder="">
                                </div>
                            </div>
                            
                             <!-- checkbox -->
                            <div class="col-sm-offset-3 col-sm-9">
                                <div class="form-group">
                                    <label>
                                        <input type="checkbox" class="minimal" checked>
                                    </label>
                                    Paid
                                </div>
                            </div>
                        </div>
                        <!-- /.box-body -->
                        
                        <!-- /.box-footer -->
                        <div class="box-footer">
                            <button type="submit" class="btn btn-info bg-red">Cancel</button>
                            <button type="submit" class="btn btn-info pull-right bg-green">Save</button>
                        </div>
                        <!-- /.box-footer -->
                    </form>
                </div>
            </div>
    
        </div>
        <!-- /.row -->
    
        <div class="row justify-content-center">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Dashboard</div>
    
                    <div class="panel-body">
                        @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                        @endif
    
                        You are logged in!
                    </div>
                </div>
            </div>
        </div>
    </div>    

@stop

@section('js')
    <!-- InputMask -->
    <script src="{!! asset('/plugins/input-mask/inputmask.js') !!}"></script>
    <script src="{!! asset('/plugins/input-mask/jquery.inputmask.js') !!}"></script>
    <script src="{!! asset('/plugins/input-mask/inputmask.extensions.js') !!}"></script>
    <script src="{!! asset('/plugins/input-mask/inputmask.date.extensions.js') !!}"></script>
    <script src="{!! asset('/plugins/input-mask/inputmask.numeric.extensions.js') !!}"></script>
    
    <!-- iCheck 1.0.1 -->
    <script src="{!! asset('/plugins/iCheck/icheck.min.js') !!}"></script>
    
    <!-- Page script -->
    <script>
        $(function () {
            //Initialize Select2 Elements
            $('.select2').select2()

            //Datemask dd/mm/yyyy
            $('#datemask').inputmask('datetime', { 
                inputFormat: " dd/mm/yyyy"            
            });

            $("#money").inputmask("currency", {
                radixPoint: ".",
                groupSeparator: ",",
                digits: 2,
                autoGroup: true,
                prefix: ' BRL ', //Space after $, this will not truncate the first character.
                rightAlign: false,
                clearMaskOnLostFocus: true
            });

            //iCheck for checkbox and radio inputs
            $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
                checkboxClass: 'icheckbox_minimal-red',
                radioClass   : 'iradio_minimal-red'
            })
        })
      </script>
@stop                           